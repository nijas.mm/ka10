<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User as ModelsUser;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return request()->has('phone') ? 'phone' : 'email';
    }

    public function userLogin(Request $request)
    {
        if (request()->ajax()) {
            $rules = [
                'phone'     => 'required|digits:10',
                'password'  => 'required|digits:4'
            ];
            $messages = [
                'phone.required'    => 'Phone Number Required',
                'phone.digits'    => 'Phone Number Must be 10 Digits',
                'password.required'    => 'PIN Required',
                'password.digits'    => 'PIN Must be 4 Digits',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                if (User::where('phone', $request->phone)->doesntExist()) {
                    return response()->json(['status' => false, 'message' => "Please Signup", 'signup' => 1]);
                }
                $data = User::where('phone', '=', $request->phone)->first();
                if (Hash::check($request->password, $data->password)) {
                    Auth::login($data);
                    return response()->json(['status' => true, 'message' => 'Successfully Signed in']);
                } else {
                    return response()->json(['status' => false, 'message' => 'Ohh No! Invalid PIN']);
                }
            } catch (\Throwable $th) {
                return response()->json(['status' => false, 'message' => 'Ohh No! Something went wrong!']);
            }
        }
    }
    public function userSignup(Request $request)
    {
        if (request()->ajax()) {
            $rules = [
                'phone'     => 'required|digits:10',
                'password'  => 'required|digits:4',
                'name'      => 'required'
            ];
            $messages = [
                'phone.required'    => 'Phone Number Required',
                'phone.digits'    => 'Phone Number Must be 10 Digits',
                'password.required'    => 'PIN Required',
                'password.digits'    => 'PIN Must be 4 Digits',
                'name.required' => 'Name Required'
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                if (User::where('phone', $request->phone)->exists()) {
                    return response()->json(['status' => false, 'message' => "Already Registered", 'signin' => 1]);
                }
                $data = new User();
                $data->name = $request->name;
                $data->phone = $request->phone;
                $data->password = Hash::make($request->password);
                $data->status = 1;
                $data->type = 'user';
                $data->profile_photo = 'img/user.png';
                $data->save();
                Auth::login($data);
                return response()->json(['status' => true, 'message' => 'Successfully Registered']);
            } catch (\Throwable $th) {
                return response()->json(['status' => false, 'message' => 'Ohh No! Something went wrong!']);
            }
        }
    }
}
