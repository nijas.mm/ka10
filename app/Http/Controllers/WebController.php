<?php

namespace App\Http\Controllers;

use App\Models\Careers;
use App\Models\Contents;
use App\Models\EventImages;
use App\Models\Events;
use App\Models\HomeGallery;
use App\Models\Managements;
use App\Models\News;
use App\Models\Products;
use App\Models\Settings;
use App\Models\Sliders;
use App\Models\Subscribers;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class WebController extends Controller
{   
    
    public function login()
    {
        return view('auth.login2');
    }
}
