<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menu = 'home';
        $products = Product::where('status', 1)->get();
        $transactions = Transaction::select('transactions.id', 'date', 'name', 'net_amount', 'image')
            ->where('user_id', Auth::id())
            ->where('type', 'sell')
            ->leftjoin('transaction_items as ti', 'ti.transaction_id', 'transactions.id')
            ->leftjoin('products as p', 'p.id', 'ti.product_id')->orderBy('transactions.id', 'desc')->limit('5')->get();
        $payments = Transaction::where('user_id', Auth::id())->where('type', 'payment')->limit(5)->get();
        $total_orders = Transaction::where('user_id', Auth::id())->where('type', 'sell')->count();
        $total_amount = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('gross_amount');
        $total_contribution = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('contribution');
        $total_paid = Transaction::where('user_id', Auth::id())->where('type', 'payment')->sum('net_amount');
        $total_due = $total_amount - $total_paid - $total_contribution;
        return view('app.home')->with(compact('menu', 'products', 'transactions', 'payments', 'total_orders', 'total_amount', 'total_paid', 'total_due', 'total_contribution'));
    }
    public function orders()
    {
        $menu = 'orders';
        $products = Product::where('status', 1)->get();
        $transactions = Transaction::select('transactions.id', 'date', 'name', 'net_amount', 'image')
            ->where('user_id', Auth::id())
            ->where('type', 'sell')
            ->leftjoin('transaction_items as ti', 'ti.transaction_id', 'transactions.id')
            ->leftjoin('products as p', 'p.id', 'ti.product_id')->orderBy('transactions.id', 'desc')->get();
        $payments = Transaction::where('user_id', Auth::id())->where('type', 'payment')->get();
        $total_orders = Transaction::where('user_id', Auth::id())->where('type', 'sell')->count();
        $total_amount = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('gross_amount');
        $total_contribution = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('contribution');
        $total_paid = Transaction::where('user_id', Auth::id())->where('type', 'payment')->sum('net_amount');
        $total_due = $total_amount - $total_paid - $total_contribution;
        return view('app.orders')->with(compact('menu', 'products', 'transactions', 'payments', 'total_orders', 'total_amount', 'total_paid', 'total_due', 'total_contribution'));
    }
    public function payments()
    {
        $menu = 'payments';
        $products = Product::where('status', 1)->get();
        $transactions = Transaction::select('transactions.id', 'date', 'name', 'net_amount', 'image')
            ->where('user_id', Auth::id())
            ->where('type', 'sell')
            ->leftjoin('transaction_items as ti', 'ti.transaction_id', 'transactions.id')
            ->leftjoin('products as p', 'p.id', 'ti.product_id')->orderBy('transactions.id', 'desc')->get();
        $payments = Transaction::where('user_id', Auth::id())->where('type', 'payment')->get();
        $total_orders = Transaction::where('user_id', Auth::id())->where('type', 'sell')->count();
        $total_amount = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('gross_amount');
        $total_contribution = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('contribution');
        $total_paid = Transaction::where('user_id', Auth::id())->where('type', 'payment')->sum('net_amount');
        $total_due = $total_amount - $total_paid - $total_contribution;
        return view('app.payments')->with(compact('menu', 'products', 'transactions', 'payments', 'total_orders', 'total_amount', 'total_paid', 'total_due', 'total_contribution'));
    }
    public function items()
    {
        $menu = 'items';
        $products = Product::where('status', 1)->get();
        $transactions = Transaction::select('transactions.id', 'date', 'name', 'net_amount', 'image')
            ->where('user_id', Auth::id())
            ->where('type', 'sell')
            ->leftjoin('transaction_items as ti', 'ti.transaction_id', 'transactions.id')
            ->leftjoin('products as p', 'p.id', 'ti.product_id')->orderBy('transactions.id', 'desc')->get();
        $payments = Transaction::where('user_id', Auth::id())->where('type', 'payment')->get();
        $total_orders = Transaction::where('user_id', Auth::id())->where('type', 'sell')->count();
        $total_amount = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('gross_amount');
        $total_contribution = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('contribution');
        $total_paid = Transaction::where('user_id', Auth::id())->where('type', 'payment')->sum('net_amount');
        $total_due = $total_amount - $total_paid - $total_contribution;
        return view('app.items')->with(compact('menu', 'products', 'transactions', 'payments', 'total_orders', 'total_amount', 'total_paid', 'total_due', 'total_contribution'));
    }
    public function settings()
    {
        $menu = 'settings';
        return view('app.settings')->with(compact('menu'));
    }
    public function products()
    {
        $menu = 'products';
        $products = Product::get();
        $transactions = Transaction::select('transactions.id', 'date', 'name', 'net_amount', 'image')
            ->where('user_id', Auth::id())
            ->where('type', 'sell')
            ->leftjoin('transaction_items as ti', 'ti.transaction_id', 'transactions.id')
            ->leftjoin('products as p', 'p.id', 'ti.product_id')->orderBy('transactions.id', 'desc')->get();
        $payments = Transaction::where('user_id', Auth::id())->where('type', 'payment')->get();
        $total_orders = Transaction::where('user_id', Auth::id())->where('type', 'sell')->count();
        $total_amount = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('gross_amount');
        $total_contribution = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('contribution');
        $total_paid = Transaction::where('user_id', Auth::id())->where('type', 'payment')->sum('net_amount');
        $total_due = $total_amount - $total_paid - $total_contribution;
        return view('app.products')->with(compact('menu', 'products', 'transactions', 'payments', 'total_orders', 'total_amount', 'total_paid', 'total_due', 'total_contribution'));
    }
    public function darkmodeSwitch($mode)
    {
        $user = User::find(Auth::id());
        $user->dark_mode = $mode;
        $user->save();
    }

    public function createProduct(Request $request)
    {
        $rules = [
            'name'          => 'required',
            'price'         => 'required|integer',
            'status'         => 'required',
            'availability'         => 'required',
            'image'          => 'required'
        ];
        $messages = [
            'name.required'    => 'Name Required',
            'price.required'            => 'Price required',
            'image.required'          => 'Image required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        if ($request->hasFile('image')) {
            $path = public_path('/uploads/product_images');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $imageName =  uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('/uploads/product_images'), $imageName);
            $image =  '/uploads/product_images/' . $imageName;
        }
        try {
            $product = new product();
            $product->name = $request->name;
            $product->image = $image;
            $product->price = $request->price;
            $product->status = $request->status;
            $product->availability = $request->availability;
            $product->save();
            return response()->json(['status' => true, 'message' => "Succussfully Added!"]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    public function allOrders(Request $request)
    {
        $menu = 'all_orders';
        $products = Product::get();
        $total_amount = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('gross_amount');
        $total_contribution = Transaction::where('user_id', Auth::id())->where('type', 'sell')->sum('contribution');
        $total_paid = Transaction::where('user_id', Auth::id())->where('type', 'payment')->sum('net_amount');
        $total_due = $total_amount - $total_paid - $total_contribution;
        $date = date('Y-m-d');
        if (date('H') <= 12) {
            $time = 'FN';
        } else {
            $time = 'AN';
        }
        $transactions = Transaction::leftjoin('transaction_items', 'transaction_items.transaction_id', 'transactions.id')
            ->leftjoin('products', 'products.id', 'transaction_items.product_id')
            ->leftjoin('users', 'users.id', 'transactions.user_id');
        if ($request->get('date')) {
            $transactions = $transactions->whereDate('date', $request->get('date'));
            $date = $request->get('date');
        }
        if ($request->get('time') && $request->get('time') == 'FN') {
            $transactions = $transactions->whereTime('date', '<', '12:00');
            $time = 'FN';
        }
        if ($request->get('time') && $request->get('time') == 'AN') {
            $transactions = $transactions->whereTime('date', '>', '12:00');
            $time = 'AN';
        }
        $transactions = $transactions->select('transactions.id', 'date', 'invoice_no', 'gross_amount', 'contribution', 'net_amount', 'products.name as product', 'users.name as user', 'qty');
        $transactions = $transactions->simplepaginate(200);


        $overview = Transaction::leftjoin('transaction_items', 'transaction_items.transaction_id', 'transactions.id')
            ->leftjoin('products', 'products.id', 'transaction_items.product_id')
            ->leftjoin('users', 'users.id', 'transactions.user_id');
        if ($request->get('date')) {
            $overview = $overview->whereDate('date', $request->get('date'));
            $date = $request->get('date');
        }
        if ($request->get('time') && $request->get('time') == 'FN') {
            $overview = $overview->whereTime('date', '<', '12:00');
            $time = 'FN';
        }
        if ($request->get('time') && $request->get('time') == 'AN') {
            $overview = $overview->whereTime('date', '>', '12:00');
            $time = 'AN';
        }
        $overview = $overview->select('products.name as product', DB::raw('SUM(qty) as qty'));
        $overview = $overview->groupBy('product_id')->get();
        return view('app.all_orders')->with(compact('menu', 'products', 'transactions', 'total_amount', 'total_paid', 'total_due', 'total_contribution', 'date', 'time', 'overview'));
    }
}
