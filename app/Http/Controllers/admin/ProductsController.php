<?php

namespace App\Http\Controllers\admin;

// use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProductsController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::select('id', 'name', 'image', 'price', 'status');
            // dd($data); 
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                    return '<img src="' . asset($row->image) . '" style="height:50px" alt="' . $row->name . '">';
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<span class="badge badge-success">ACTIVE</span>';
                    } else {
                        return '<span class="badge badge-danger">IN ACTIVE</span>';
                    }
                })
                ->addColumn('action', function ($row) {
                    $button = '<button type="button" data-href="' . route('products.edit', $row->id) . '" data-keyboard="false" data-backdrop="static" class="btn btn-xs btn-primary edit_button" data-container=".modal_class"><i class="fa fa-edit"></i></button>&nbsp';
                    $button .= '<button data-href="' . route('delete', ['products', $row->id]) . '" class="btn btn-xs btn-danger btn-modal"><i class="fa fa-trash"></i></button>';
                    return $button;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->Where('name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['status', 'action', 'image'])
                ->make(true);
        }
        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }
    public function getProductValue(Request $request)
    {
        $prodId = $request->prodId;
        $product = Product::find($prodId);
        return response()->json(['status' => true, 'price' => $product->price]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $rules = [
            'name'          => 'required',
            'price'         => 'required|integer',
            'status'         => 'required',
            'image'          => 'required|mimes:jpg,jpeg,png,gif'
        ];
        $messages = [
            'name.required'    => 'Name Required',
            'price.required'            => 'Price required',
            'image.required'          => 'Image required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
        }
        if ($request->hasFile('image')) {
            $path = public_path('/uploads/product_images');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $imageName =  uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('/uploads/product_images'), $imageName);
            $image =  '/uploads/product_images/' . $imageName;
        }
        try {

            $product = new product();
            $product->name = $request->name;
            $product->image = $image;
            $product->price = $request->price;
            $product->status = $request->status;
            $product->save();

            return ['success' => true, 'msg' => "succussfully added"];
        } catch (\Exception $e) {
            return ['success' => false, 'msg' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (request()->ajax()) {
            $product = Product::find($id);
            return view('admin.products.edit')->with(compact('product'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->ajax()) {
            try {
                if ($request->hasFile('image')) {
                    $validator = Validator::make($request->all(), [
                        'image' => 'mimes:jpg,jpeg,png,gif',
                    ], $messages = [
                        'mimes' => 'Please insert image only'
                    ]);
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator)->withInput();
                    }
                    $product = Product::findOrFail($id);
                    $path = public_path() . $product->image;
                    if (file_exists($path)) {
                        unlink($path);
                    }
                    $path = public_path('/uploads/product_images');
                    if (!file_exists($path)) {
                        File::makeDirectory($path, $mode = 0777, true, true);
                    }
                    $imageName =  uniqid() . '.' . $request->image->extension();
                    $request->image->move(public_path('/uploads/product_images'), $imageName);
                    $data = ['image' => '/uploads/product_images/' . $imageName];
                    Product::where('id', $id)->update($data);
                }
                $product = Product::findOrFail($id);
                $product->name = $request->name;
                $product->price = $request->price;
                $product->status = $request->status;
                $product->save();

                return response()->json(['success' => true, 'msg' => 'Successfully Updated!']);
            } catch (\Exception $e) {
                return response()->json(['success' => false, 'msg' => 'Something went wrong, please try again']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->ajax()) {
            try {
                $product = Product::findOrFail($id);
                $product->delete();
                return response()->json(['success' => true, 'msg' => 'successfully deleted']);
            } catch (\Exception $e) {
                return response()->json(['success' => false, 'msg' => $e->getMessage()]);
            }
        }
    }
}
