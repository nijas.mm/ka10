<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PaymentsController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        if ($request->ajax()) {
            $data = UserTransaction::select('user_transactions.id','user_transactions.date','user_transactions.amount','user_transactions.type','user_transactions.status','user_transactions.remarks','users.profile_photo','name')->leftjoin('users','users.id','=','user_transactions.user_id');
            if(Auth::user()->type=='user'){
                $data = $data->where('user_id',Auth::id());
            }
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {
                        $button = '<button type="button" data-href="'.route('payments.edit',$row->id).'" data-keyboard="false" data-backdrop="static" class="btn btn-xs btn-primary edit_button" data-container=".modal_class"><i class="fa fa-edit"></i></button>&nbsp';
                        $button .='<button data-href="'.route('delete',['payments',$row->id]).'" class="btn btn-xs btn-danger btn-modal"><i class="fa fa-trash"></i></button>';
                        return $button;
                    })
                    ->addColumn('profile_photo',function ($row) {
                        if($row->profile_photo){
                            return '<img src="'.asset($row->profile_photo).'" style="height:50px" alt="'.$row->name.'"> '.$row->name;
                        }else{
                            return '<img src="'.asset('img/user2.png').'" style="height:50px" alt="'.$row->name.'"> '.$row->name;
                        }
                        
                    })
                    ->addColumn('status',function($row){
                        if($row->status==2){
                            return '<span class="badge badge-warning">PENDING</span>';
                        }else{
                            return '<span class="badge badge-success">CONFIRMED</span>';
                        }
                    })
                    ->filter(function ($instance) use ($request) {
                        if (!empty($request->get('search'))) {
                             $instance->where(function($w) use($request){
                                $search = $request->get('search');
                                $w->Where('type', 'LIKE', "%$search%");
                            });
                        }
                    })
                    ->rawColumns(['status','action','profile_photo'])
                    ->make(true);
        }
        return view('admin.payments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $users = User::where('status',1)->get();
        return view('admin.payments.create')->with(compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->type=='admin'){
            $rules = ['user_id'=> 'required','status'=>'required'];
        }
        $rules = [
            'amount'         => 'required|integer',    
            'remarks'         => 'required'
        ];
        $messages = [
            'amount.required'    => 'Name Required'
        ];
        $validator = Validator::make(request()->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
        }
        try {
            $payment = new UserTransaction();
            $payment->transaction_id = 1;
            $payment->date = now();
            $payment->amount = $request->amount;
            $payment->remarks = $request->remarks;
            $payment->type = 'credit';
            if(Auth::user()->type=='admin'){
                $payment->user_id = $request->user_id;
                $payment->status = $request->status;
            }else{
                $payment->user_id = Auth::id();
                $payment->status = 2;
            }
            $payment->save();
            return ['success' => true,'msg' => "succussfully added"];
        } catch (\Exception $e) {
            return ['success' => false,'msg' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (request()->ajax()) {
            $users = User::where('status',1)->get();
            $payment = UserTransaction::find($id); 
            return view('admin.payments.edit')->with(compact('users','payment'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        if (request()->ajax()) {
            if(Auth::user()->type=='admin'){
                $rules = ['user_id'=> 'required','status'=>'required'];
            }
            $rules = [
                'amount'         => 'required|integer',    
                'remarks'         => 'required'
            ];
            $messages = [
                'amount.required'    => 'Name Required'
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
            }
            try {
                $payment = UserTransaction::findOrFail($id); 
                $payment->transaction_id = 1;
                $payment->amount = $request->amount;
                $payment->remarks = $request->remarks;
                $payment->type = 'credit';
                if(Auth::user()->type=='admin'){
                    $payment->user_id = $request->user_id;
                    $payment->status = $request->status;
                }else{
                    $payment->user_id = Auth::id();
                    $payment->status = 2;
                }
                $payment->save();
                
                return ['success' => true,'msg' =>'successfully updated'];
            } catch (\Exception $e) {
                return ['success' => false, 'msg' => 'Something went wrong, please try again'];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        if (request()->ajax()) {
            try {
                $payment = Transaction::findOrFail($id);
                $payment->delete();
                return response()->json(['success' => true,'msg' =>'successfully deleted']);
            } catch (\Exception $e) {
                return response()->json(['success' => false,'msg' => $e->getMessage()]);
            }
        }
    }
}
