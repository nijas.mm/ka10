<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Artisan::call('up');
        if ($request->ajax()) {
            $data = User::select('id', 'profile_photo', 'name', 'email', 'phone', 'status', 'type');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $button = '<a href="' . url('admin/users') . '/' . $row->id . '/edit" class="btn btn-xs btn-outline-primary"><i class="fa fa-edit"></i></a>&nbsp';
                    if (Auth::id() != $row->id) {
                        $button .= '<button data-href="' . route('delete', ['users', $row->id]) . '" class="btn btn-xs btn-outline-danger btn-modal"><i class="fa fa-trash"></i></button>';
                    }
                    return $button;
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<span class="badge badge-success">ACTIVE</span>';
                    } else {
                        return '<span class="badge badge-danger">IN ACTIVE</span>';
                    }
                })
                ->addColumn('profile_photo', function ($row) {
                    if ($row->profile_photo) {
                        return '<img src="' . asset($row->profile_photo) . '" style="height:50px" alt="' . $row->name . '">';
                    } else {
                        return '<img src="' . asset('img/user2.png') . '" style="height:50px" alt="' . $row->name . '">';
                    }
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->Where('name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['status', 'action', 'profile_photo'])
                ->make(true);
        }
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'type'                              => 'required',
            'name'                              => 'required|string|min:3|max:30',
            'phone'                             => 'required|min:10|max:10|unique:users',
            'status'                            => 'required',
            'password'                          => 'required|confirmed|min:4',
            'password_confirmation'             => 'required',
        ];
        $messages = [
            'name.required'    => 'Name is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        try {
            $user = new User();
            $user->type = $request->type;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->status = $request->status;
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect('admin/users')->with(['status' => 'success', 'message' => "USER ADDED SUCCESSFULLY"]);
        } catch (\Throwable $e) {
            return redirect()->back()->withErrors(['message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'type'                              => 'required',
            'name'                              => 'required|string|min:3|max:30',
            'phone'                             => 'required|min:10|max:10|unique:users,phone,' . $id,
            'status'                            => 'required',
        ];
        if ($request->password) {
            $rules = [
                'password'                      => 'required|confirmed|min:6',
                'password_confirmation'         => 'required',
            ];
        }
        $messages = [
            'name.required'    => 'Name is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        try {
            $user_data = [
                'type'      => $request->type,
                'name'      => $request->name,
                'phone'     => $request->phone,
                'status'    => $request->status,
            ];
            User::where('id', $id)->update($user_data);
            return redirect()->back()->with(['status' => 'success', 'message' => 'USER UPDATED SUCCESSFULLY!']);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::id() == $id) {
            return response()->json(['success' => false, 'msg' => 'Active User cannot be deleted']);
        }
        if (request()->ajax()) {
            try {
                $user = User::findOrFail($id);
                $user->delete();
                return response()->json(['success' => true, 'msg' => 'successfully deleted']);
            } catch (\Exception $e) {
                return response()->json(['success' => false, 'msg' => $e->getMessage()]);
            }
        }
    }

    public function getUserRoleName($user_id)
    {
        $user = User::findOrFail($user_id);
        $roles = $user->getRoleNames();
        $role_name = '';
        if (!empty($roles[0])) {
            $role_name = !empty($roles[0]) ? $roles[0] : '';
        }
        return $role_name;
    }
}
