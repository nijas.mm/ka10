<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Transaction::select('id', 'date', 'invoice_no', 'gross_amount', 'contribution', 'net_amount', 'status');
            if (Auth::user()->type == 'user') {
                $data = $data->where('user_id', Auth::id());
            }
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $button = '<button type="button" data-href="' . route('orders.edit', $row->id) . '" data-keyboard="false" data-backdrop="static" class="btn btn-xs btn-primary edit_button" data-container=".modal_class"><i class="fa fa-edit"></i></button>&nbsp';
                    $button .= '<button data-href="' . route('delete', ['orders', $row->id]) . '" class="btn btn-xs btn-danger btn-modal"><i class="fa fa-trash"></i></button>';
                    return $button;
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<span class="badge badge-warning">PENDING</span>';
                    } else {
                        return '<span class="badge badge-success">CONFIRMED</span>';
                    }
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->Where('name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }
        $row_count = isset($request->row_count) ? $request->row_count : 1;
        return view('admin.orders.index', compact('row_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('status', 1)->get();
        $products = Product::get();
        $row_count = isset($request->row_count) ? $request->row_count : 1;
        return view('admin.orders.create')->with(compact('users', 'products', 'row_count'));
    }
    public function getAppendedRows(Request $request)
    {
        $row_count = isset($request->row_count) ? $request->row_count : 1;
        $data['products'] = Product::get();
        $data['row_count'] = $row_count;
        $html = view('admin.orders.row', $data)->render();
        return response()->json(['status' => true, 'html' => $html]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'product_id'     => 'required|array|min:1',
        ];
        $messages = [
            'product_id.required'    => 'Item Required',
        ];
        if (Auth::user()->type == 'admin') {
            $rules = [
                'user_id'     => 'required',

            ];
            $messages = [
                'name.required'    => 'User Required',
            ];
            $user_id = $request->user_id;
        } else {
            $user_id = Auth::id();
        }
        $validator = Validator::make(request()->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
        }
        try {
            if (Transaction::whereDate('date', date('Y-m-d'))->exists()) {
                $contribution = 0;
            } else {
                $contribution = 10;
            }
            DB::beginTransaction();
            $order = new Transaction();
            $order->type = 'sell';
            $order->date = now();
            $order->invoice_no = rand(100000, 999999);
            $order->gross_amount = 0;
            $order->contribution = $contribution;
            $order->net_amount = 0;
            $order->user_id = $user_id;
            $order->created_by = Auth::id();
            $order->updated_by = Auth::id();
            $order->status = 1;
            $order->save();
            $gross_amount = 0;
            foreach ($request->product_id as $key => $value) {
                $rate = Product::find($value)['price'];
                $item = new TransactionItem();
                $item->transaction_id = $order->id;
                $item->product_id = $value;
                $item->rate = $rate;
                $item->qty = $request->qty[$key];
                $item->total = $request->qty[$key] * $rate;
                $item->save();
                $gross_amount += $request->qty[$key] * $rate;
            }
            $order->gross_amount = $gross_amount;
            $order->net_amount = $gross_amount - $order->contribution;
            $order->save();
            DB::commit();
            return response()->json(['status' => true, 'message' => "Succussfully Added!", 'data' => $order]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $orders
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (request()->ajax()) {
            $orders = Transaction::find($id);
            return view('admin.orders.edit')->with(compact('orders'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->ajax()) {
            try {
                $input = $request->only([
                    'name',
                ]);
                $orders = Transaction::findOrFail($id);
                $orders->name = $input['name'];
                $orders->save();

                return ['success' => true, 'msg' => 'successfully updated'];
            } catch (\Exception $e) {
                return ['success' => false, 'msg' => 'Something went wrong, please try again'];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->ajax()) {
            try {
                $orders = Transaction::findOrFail($id);
                $orders->delete();
                return response()->json(['success' => true, 'msg' => 'successfully deleted']);
            } catch (\Exception $e) {
                return response()->json(['success' => false, 'msg' => $e->getMessage()]);
            }
        }
    }

    public function appOrder(Request $request)
    {
        if (request()->ajax()) {
            $rules = [
                'product_id'     => 'required',
                'qty'  => 'required'
            ];
            $messages = [
                'product_id.required'    => 'Phone Number Required',
                'qty.required' => 'Quantity Required'
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                if (Transaction::whereDate('date', date('Y-m-d'))->where('user_id', Auth::id())->exists()) {
                    $contribution = 0;
                } else {
                    $contribution = 10;
                }
                DB::beginTransaction();
                $order = new Transaction();
                $order->type = 'sell';
                $order->date = now();
                $order->invoice_no = rand(100000, 999999);
                $order->gross_amount = 0;
                $order->contribution = $contribution;
                $order->net_amount = 0;
                $order->user_id = Auth::id();
                $order->created_by = Auth::id();
                $order->updated_by = Auth::id();
                $order->status = 1;
                $order->save();
                $gross_amount = 0;


                $rate = Product::find($request->product_id)['price'];
                $item = new TransactionItem();
                $item->transaction_id = $order->id;
                $item->product_id = $request->product_id;
                $item->rate = $rate;
                $item->qty = $request->qty;
                $item->total = $request->qty * $rate;
                $item->save();

                $order->gross_amount = $item->total;
                $order->net_amount = $item->total - $order->contribution;
                $order->save();

                DB::commit();
                return response()->json(['status' => true, 'message' => "Order Placed!"]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'msg' => $e->getMessage()]);
            }
        }
    }
}
