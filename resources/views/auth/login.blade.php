@include('auth.head')
<!-- App Header -->
    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="#" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"></div>
        <div class="right">
            <a href="{{route('register')}}" class="headerButton">
                Register
            </a>
        </div>
    </div>
    <!-- * App Header -->
    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="section mt-2 text-center">
            <h1>TECHTEA</h1>
            <h4>Login! Order! Have Fun!</h4>
        </div>
        <div class="section mb-5 p-2">

            <form method="POST" id="phoneLogin">
              @csrf
                <div class="card">
                    <div class="card-body pb-1">
                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="phone">Phone</label>
                                <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" id="phone"  name="phone" placeholder="Your Phone" maxlength="10" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group basic">
                            <div class="input-wrapper">
                                <label class="label" for="password">PIN</label>
                                <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" id="password" name="password" autocomplete="off"
                                    placeholder="Your PIN" maxlength="4" required>
                                <i class="clear-input">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-links mt-2">
                    <div>
                        Not registered yet? <a href="{{route('register')}}">Register Now</a>
                    </div>
                </div>
                <div class="form-button-group  transparent">
                    <button type="submit" class="btn btn-primary btn-block btn-lg save_button">Get me In!</button>
                </div>
            </form>
        </div>

    </div>
    <!-- * App Capsule -->
    
@include('auth.footer')
<script>
      $("#phoneLogin").unbind('submit').on('submit', function (e) {
      $('.save_button').text('Please Wait...!');
      $(this).find('button[type="submit"]').attr('disabled', true);
      e.preventDefault();
      var data = $(this).serialize();
      $.ajax({
          method: 'POST',
          url: "user-login",
          dataType: 'json',
          data: data,
          success: function(result) {
              if (result.status === true) {
                  toastr.success(result.message);
                  window.location.href = "/";
              } else {
                  toastr.error(result.message);
                  if (result.signup === 1) {
                    window.setTimeout(function(){
                      window.location.href = "/register";
                    }, 3000);
                  }
                  $('.save_button').attr('disabled', false);
                  $('.save_button').text('Get me In!');
              }
          },
      });
  });
    </script>