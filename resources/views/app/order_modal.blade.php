<!-- Order Action Sheet -->
        <div class="modal fade action-sheet" id="orderActionSheet" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Order</h5>
                    </div>
                    <div class="modal-body">
                        <div class="action-sheet-content">
                            <form id="order-form">
                              @csrf
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="account1">Item</label>
                                        <select class="form-control custom-select" id="product_id" name="product_id">
                                            <option value="">SELECT ITEM</option>
                                            @foreach ($products as $item)
                                                <option value="{{$item->id}}">{{$item->name}} - ₹ {{$item->price}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group basic">
                                    <label class="label">Quantity</label>
                                    <select class="form-control custom-select" id="qty" name="qty">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                        </select>
                                </div>
                                <div class="form-group basic">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg save_button">Order</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Order Action Sheet -->