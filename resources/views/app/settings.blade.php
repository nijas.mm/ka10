@include('app.app_header')

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="section mt-3 text-center">
            <div class="avatar-section">
                <a href="#">
                    <img src="{{Auth::user()->profile_photo}}" alt="avatar" class="imaged w100 rounded">
                    <span class="button">
                        <ion-icon name="camera-outline"></ion-icon>
                    </span>
                </a>
            </div>
        </div>

        <div class="listview-title mt-1">Theme</div>
        <ul class="listview image-listview text inset no-line">
            <li>
                <div class="item">
                    <div class="in">
                        <div>
                            Dark Mode
                        </div>
                        <div class="form-check form-switch  ms-2">
                            <input class="form-check-input dark-mode-switch" type="checkbox" id="darkmodeSwitch" onchange="darkmodeSwitch()">
                            <label class="form-check-label" for="darkmodeSwitch"></label>
                        </div>
                    </div>
                </div>
            </li>
        </ul>

        <div class="listview-title mt-1">Notifications</div>
        <ul class="listview image-listview text inset">
            <li>
                <div class="item">
                    <div class="in">
                        <div>
                            Payment Alert
                            <div class="text-muted">
                                Send notification when there is a new payment
                            </div>
                        </div>
                        <div class="form-check form-switch  ms-2">
                            <input class="form-check-input" type="checkbox" id="SwitchCheckDefault1">
                            <label class="form-check-label" for="SwitchCheckDefault1"></label>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="#" class="item">
                    <div class="in">
                        <div>Notification Sound</div>
                        <span class="text-primary">Beep</span>
                    </div>
                </a>
            </li>
        </ul>

        <div class="listview-title mt-1">Profile Settings</div>
        <ul class="listview image-listview text inset">
            <li>
                <a href="#" class="item">
                    <div class="in">
                        <div>{{Auth::user()->name}}</div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#" class="item">
                    <div class="in">
                        <div>{{Auth::user()->phone}}</div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#" class="item">
                    <div class="in">
                        <div>{{Auth::user()->email}}</div>
                        <span class="text-primary">Edit</span>
                    </div>
                </a>
            </li>
        </ul>

        <div class="listview-title mt-1">Security</div>
        <ul class="listview image-listview text mb-2 inset">
            <li>
                <a href="#" class="item">
                    <div class="in">
                        <div>Update PIN</div>
                    </div>
                </a>
            </li>
            <li>
                <a href="javascript:void" class="item" onclick="$('#logout-form').submit();">
                    <div class="in">
                        <div>Log out</div>
                    </div>
                </a>
            </li>
        </ul>


    </div>
    <!-- * App Capsule -->


    @include('app.bottom_menu')
    
    @include('app.footer')
    <script>
        function darkmodeSwitch(){
            if($('#darkmodeSwitch').is(':checked')==true){
                var checked = 1;
            }else{
                var checked = 0;
            }
            $.ajax({
            method: 'GET',
            url: "darkmode-switch/"+checked,
            dataType: 'json',
            success: function(result) {
                if (result.status === true) {
                    toastr.success(result.message);
                    window.setTimeout(function(){
                        window.location.href = "/";
                        }, 3000);
                } else {
                    toastr.error(result.message);
                    $('.save_button').attr('disabled', false);
                    $('.save_button').text('Get me In!');
                }
            },
        });
        }
    </script>
</body>

</html>