<!-- App Bottom Menu -->
    <div class="appBottomMenu">
        <a href="{{route('home')}}" class="item @if($menu=='home') active @endif">
            <div class="col">
                <ion-icon name="pie-chart-outline"></ion-icon>
                <strong>Overview</strong>
            </div>
        </a>
        <a href="{{route('orders')}}" class="item @if($menu=='orders') active @endif">
            <div class="col">
                <ion-icon name="document-text-outline"></ion-icon>
                <strong>Orders</strong>
            </div>
        </a>
        <a href="{{route('items')}}" class="item @if($menu=='items') active @endif">
            <div class="col">
                <ion-icon name="apps-outline"></ion-icon>
                <strong>Items</strong>
            </div>
        </a>
        <a href="{{route('payments')}}" class="item @if($menu=='payments') active @endif">
            <div class="col">
                <ion-icon name="card-outline"></ion-icon>
                <strong>Payments</strong>
            </div>
        </a>
        <a href="{{route('settings')}}" class="item @if($menu=='settings') active @endif">
            <div class="col">
                <ion-icon name="settings-outline"></ion-icon>
                <strong>Settings</strong>
            </div>
        </a>
    </div>
    <!-- * App Bottom Menu -->