@include('app.app_header')
    <!-- App Capsule -->
<div id="appCapsule">
    <!-- Wallet Card -->
        <div class="section wallet-card-section pt-1">
            <div class="wallet-card">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">List of Items</span>
                    </div>
                    <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#productCreateModal">
                            <ion-icon name="add-outline"></ion-icon>
                        </a>
                    </div>
                </div>
                <!-- * Balance -->
            </div>
        </div>
        @include('app.product_modal')
        <div class="section mt-2 mb-2">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">NAME</th>
                                <th scope="col">IMAGE</th>
                                <th scope="col">PRICE</th>
                                <th scope="col">AVAILABILITY</th>
                                <th scope="col">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $item)
                            <tr>
                                <td>{{$item->name}}</td>
                                <td><img src="{{$item->image}}" alt="{{$item->name}}" style="width: 50px"></td>
                                <td>{{$item->price}}</td>
                                <td>{{$item->availability}}</td>
                                <td>@if($item->status==1) <p class="text-success">ACTIVE</p> @else<p class="text-danger"> INACTIVE </p>@endif </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        </div>
    <!-- * App Capsule -->
    @include('app.bottom_menu')
    @include('app.sidebar')
    @include('app.footer')
    <script>
        //CREATE FORM APPEND TO modal_class AND POST ACTIONS
      $(document).unbind('submit').on('submit', 'form#createForm', function(e){
      e.preventDefault();
      $(this).find('button[type="submit"]').attr('disabled', true);
      var form = $('form#createForm')[0];
      var formData = new FormData(form);
      $.ajax({
        method: "post",
        url: $(this).attr("action"), 
        headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}",
                },
        dataType: "json", 
        data: formData, 
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
          success:function(result){
              if(result.status == true){
                $('div.modal_class').modal('hide');
                  toastr.success(result.message);
                  window.setTimeout(function(){
                    window.location.href = "/products";
                    }, 3000);
              }else{
                    toastr.error(result.message);
                    $('.save_button').attr('disabled', false);
              }
          }
      });
    });
    </script>
</body>
</html>