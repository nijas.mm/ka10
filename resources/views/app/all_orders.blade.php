@include('app.app_header')
    <!-- App Capsule -->
<div id="appCapsule">
    <!-- Wallet Card -->
        <div class="section wallet-card-section pt-1">
            <div class="wallet-card">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">List of Orders</span>
                        <form action="{{route('all-orders')}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="date" value="{{$date}}">
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control custom-select" id="time" name="time">
                                        <option value="FN" @if($time=='FN') selected @endif>FOR NOON</option>
                                        <option value="AN" @if($time=='AN') selected @endif>AFTER NOON</option>
                                    </select>
                                </div>
                            </div>
                            
                            
                             <div class="form-group basic">
                                <button type="submit" class="btn btn-primary btn-block btn-xs save_button">SEARCH</button>
                            </div>
                        </form>
                    </div>
                    <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#orderCreateModal">
                            <ion-icon name="add-outline"></ion-icon>
                        </a>
                    </div>
                </div>
                <!-- * Balance -->
            </div>
        </div>
        @include('app.admin_order_modal')
        @if(isset($overview) && count($overview)>0)
        <div class="section mt-2 mb-2">
            <div class="card">
                <div class="table-responsive">
                    <table class="table bg-primary" id="order-table">
                        <thead>
                            <tr>
                                <th scope="col">ITEM</th>
                                <th scope="col">QUANTITY</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($overview as $item)
                            <tr>
                                <td>{{$item->product}}</td>
                                <td>{{$item->qty}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        @endif
        <div class="section mt-2 mb-2">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped" id="order-table">
                        <thead>
                            <tr>
                                <th scope="col">DATE & SECTION</th>
                                <th scope="col">USER</th>
                                <th scope="col">ITEM</th>
                                <th scope="col">QUANTITY</th>
                                <th scope="col">GROSS AMOUNT</th>
                                <th scope="col">CONTRIBUTION</th>
                                <th scope="col">NET AMOUNT</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transactions as $item)
                            <tr>
                                <td>{{$item->date}}</td>
                                <td>{{$item->user}}</td>
                                <td>{{$item->product}}</td>
                                <td>{{$item->qty}}</td>
                                <td>{{$item->gross_amount}}</td>
                                <td>{{$item->contribution}}</td>
                                <td>{{$item->net_amount}}</td>
                            </tr>
                            @endforeach
                            @if(count($transactions)==0)
                            <tr>
                                <td colspan="7" align="center">No Orders Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <br>
                    {{ $transactions->links() }}
                    <br>
                </div>

            </div>
        </div>
        </div>
    <!-- * App Capsule -->
    @include('app.bottom_menu')
    @include('app.sidebar')
    @include('app.footer')
</body>
</html>