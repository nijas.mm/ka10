<!-- App Sidebar -->
    <div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <!-- profile box -->
                    <div class="profileBox pt-2 pb-2">
                        <div class="image-wrapper">
                            <img src="{{Auth::user()->profile_photo}}" alt="image" class="imaged  w36">
                        </div>
                        <div class="in">
                            <strong>{{Auth::user()->name}}</strong>
                            <div class="text-muted">{{Auth::user()->phone}}</div>
                        </div>
                        <a href="#" class="btn btn-link btn-icon sidebar-close" data-bs-dismiss="modal">
                            <ion-icon name="close-outline"></ion-icon>
                        </a>
                    </div>
                    <!-- * profile box -->
                    <!-- balance -->
                    <div class="sidebar-balance">
                        <div class="listview-title">Total Due</div>
                        <div class="in">
                            <h1 class="amount">₹ {{$total_due}}</h1>
                        </div>
                    </div>
                    <!-- * balance -->

                    <!-- menu -->
                    <div class="listview-title mt-1">Menu</div>
                    <ul class="listview flush transparent no-line image-listview">
                        <li>
                            <a href="{{route('home')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="pie-chart-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Overview
                                    <span class="badge badge-primary">10</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('orders')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="document-text-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    My Orders
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('items')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="apps-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Items
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('payments')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="card-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Payments
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- * menu -->

                    @if(Auth::user()->type=='admin')
                    <!-- Admin Menus -->
                    <div class="listview-title mt-1">Administration</div>
                    <ul class="listview flush transparent no-line image-listview">
                        <li><a href="{{route('products')}}" class="item"><div class="icon-box bg-primary"><ion-icon name="apps-outline"></ion-icon></div><div class="in">Products</div></a></li>
                        <li><a href="{{route('all-orders')}}" class="item"><div class="icon-box bg-primary"><ion-icon name="document-text-outline"></ion-icon></div><div class="in">Orders</div></a></li>
                    </ul>
                    @endif


                    <!-- others -->
                    <div class="listview-title mt-1">Others</div>
                    <ul class="listview flush transparent no-line image-listview">
                        <li>
                            <a href="{{route('settings')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="settings-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Settings
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="item" onclick="comingSoon()">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="chatbubble-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Support
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void" class="item" onclick="$('#logout-form').submit();">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="log-out-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Log out
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- * others -->
                </div>
            </div>
        </div>
    </div>
    <!-- * App Sidebar -->