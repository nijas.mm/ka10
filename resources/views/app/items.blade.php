@include('app.app_header')
    <!-- App Capsule -->
    <div id="appCapsule">

        <!-- Wallet Card -->
        <div class="section wallet-card-section pt-1">
            <div class="wallet-card">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">Total Due</span>
                        <h1 class="total text-danger">₹ {{$total_due}}</h1>
                    </div>
                    <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#orderActionSheet">
                            <ion-icon name="add-outline"></ion-icon>
                        </a>
                    </div>
                </div>
                <!-- * Balance -->
            </div>
        </div>
        <!-- Wallet Card -->

        @include('app.order_modal')

        <!-- Transactions -->
        <div class="section mt-4">
            <div class="section-heading">
                <h2 class="title">Items</h2>
            </div>
            <div class="transactions">
                <!-- item -->
                @foreach ($products as $item)
                    <a href="#" class="item" data-bs-toggle="modal" onclick="orderItem({{$item->id}})" data-bs-target="#orderActionSheet">
                    <div class="detail">
                        <img src="{{$item->image}}" alt="img" class="image-block imaged w48">
                        <div>
                            <strong>{{$item->name}}</strong>
                            <p>{{$item->name}}</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price text-success"> ₹ {{$item->price}}</div>
                    </div>
                </a>
                @endforeach
                @if((isset($products) && count($products)==0))
                No Payments
                @endif
            </div>
        </div>
        <!-- * Transactions -->

        @include('app.copyright')

    </div>
    <!-- * App Capsule -->
    @include('app.bottom_menu')
    @include('app.sidebar')
    @include('app.footer')
</body>

</html>