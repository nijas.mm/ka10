    
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <!-- ========= JS Files =========  -->
    <!-- Bootstrap -->
    <script src="assets/js/lib/bootstrap.bundle.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <!-- Splide -->
    <script src="assets/js/plugins/splide/splide.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    
    <script src="{{asset('assets/toastr.min.js')}}"></script>
    <script>
        function comingSoon(){
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.error("Coming Soon!");
        }
        function orderItem(id){
            $("#product_id").val(id).trigger('change');
        }
        $("#order-form").unbind('submit').on('submit', function (e) {
            $('.save_button').text('Please Wait...!');
            $(this).find('button[type="submit"]').attr('disabled', true);
            e.preventDefault();
            var data = $(this).serialize();
            $.ajax({
                method: 'POST',
                url: "{{route('app-order')}}",
                dataType: 'json',
                data: data,
                success: function(result) {
                    if (result.status === true) {
                        toastr.success(result.message);
                        window.setTimeout(function(){
                            window.location.href = "/";
                            }, 3000);
                    } else {
                        toastr.error(result.message);
                        $('.save_button').attr('disabled', false);
                        $('.save_button').text('Order Now!');
                    }
                },
            });
        });
    </script>