@include('app.app_header')
    <!-- App Capsule -->
    <div id="appCapsule">

        <!-- Wallet Card -->
        <div class="section wallet-card-section pt-1">
            <div class="wallet-card">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">Total Due</span>
                        <h1 class="total text-danger">₹ {{$total_due}}</h1>
                    </div>
                    <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#orderActionSheet">
                            <ion-icon name="add-outline"></ion-icon>
                        </a>
                    </div>
                </div>
                <!-- * Balance -->
            </div>
        </div>
        <!-- Wallet Card -->
        @include('app.order_modal')

        <!-- Stats -->
        <div class="section">
            <div class="row mt-2">
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Orders</div>
                        <div class="value text-danger">{{$total_orders}}</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Amount</div>
                        <div class="value text-danger">₹ {{$total_amount}}</div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Paid</div>
                        <div class="value text-success">₹ {{$total_paid}}</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Discounts</div>
                        <div class="value text-success">₹ {{$total_contribution}}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Stats -->

        <!-- Transactions -->
        <div class="section mt-4">
            <div class="section-heading">
                <h2 class="title">Orders</h2>
                <a href="{{route('orders')}}" class="link">View All</a>
            </div>
            <div class="transactions">
                <!-- item -->
                @foreach ($transactions as $item)
                    <a href="#" class="item">
                    <div class="detail">
                        <img src="{{$item->image}}" alt="img" class="image-block imaged w48">
                        <div>
                            <strong>{{$item->name}}</strong>
                            <p>{{$item->date}}</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price text-danger"> ₹ {{$item->net_amount}}</div>
                    </div>
                </a>
                @endforeach
                @if((isset($transactions) && count($transactions)==0))
                No Orders
                @endif
            </div>
        </div>
        <!-- * Transactions -->

        <!-- Transactions -->
        <div class="section mt-4">
            <div class="section-heading">
                <h2 class="title">Payments</h2>
                <a href="{{route('payments')}}" class="link">View All</a>
            </div>
            <div class="transactions">
                <!-- item -->
                @foreach ($payments as $item)
                    <a href="#" class="item">
                    <div class="detail">
                        <img src="img/paid.jpg" alt="img" class="image-block imaged w48">
                        <div>
                            <strong>Paid On</strong>
                            <p>{{$item->date}}</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price text-success"> ₹ {{$item->net_amount}}</div>
                    </div>
                </a>
                @endforeach
                @if((isset($payments) && count($payments)==0))
                No Payments
                @endif
            </div>
        </div>
        <!-- * Transactions -->

        <!-- Monthly Bills -->
        <div class="section full mt-4">
            <div class="section-heading padding">
                <h2 class="title">Available Items</h2>
                <a href="#" class="link">View All</a>
            </div>
            <!-- carousel multiple -->
            <div class="carousel-multiple splide">
                <div class="splide__track">
                    <ul class="splide__list">

                        @foreach ($products as $item)
                          <li class="splide__slide">
                            <div class="bill-box">
                                <div class="img-wrapper">
                                    <img src="{{$item->image}}" alt="img" class="image-block imaged w48">
                                </div>
                                <div class="price">₹ {{$item->price}}</div>
                                <p>{{$item->name}}</p>
                                <a href="#" class="btn btn-primary btn-block btn-sm" data-bs-toggle="modal" onclick="orderItem({{$item->id}})" data-bs-target="#orderActionSheet">ORDER</a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- * carousel multiple -->
        </div>
        <!-- * Monthly Bills -->

        @include('app.copyright')

    </div>
    <!-- * App Capsule -->


    @include('app.bottom_menu')
    @include('app.sidebar')
    @include('app.footer')
</body>

</html>