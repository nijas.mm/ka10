<!-- Order Action Sheet -->
        <div class="modal fade action-sheet" id="productCreateModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Product</h5>
                    </div>
                    <div class="modal-body">
                        <div class="action-sheet-content">
                            <form method="POST" id="createForm" action="{{route('create-product')}}" accept-charset="UTF-8" accept="multipart/form-data">
                              @csrf
                                <div class="form-group basic">
                                    <label class="label">NAME</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Item Name">
                                </div>
                                <div class="form-group basic">
                                    <label class="label">IMAGE</label>
                                    <input type="file" class="form-control" id="image" name="image" accept="image/png, image/jpeg, image/jpg">
                                </div>
                                <div class="form-group basic">
                                    <label class="label">PRICE</label>
                                    <input type="number" class="form-control" id="price" name="price" placeholder="Item Price">
                                </div>
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="account1">AVAILABILITY</label>
                                        <select class="form-control custom-select" id="availability" name="availability">
                                            <option value="">SELECT</option>
                                            <option value="FN">FOR NOON</option>
                                            <option value="AN">AFTER NOON</option>
                                            <option value="BOTH">BOTH</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="account1">STATUS</label>
                                        <select class="form-control custom-select" id="status" name="status">
                                            <option value="1">ACTIVE</option>
                                            <option value="2">INACTIVE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group basic">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg save_button">SAVE ITEM</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Order Action Sheet -->