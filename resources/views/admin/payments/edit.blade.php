<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="POST" action="{{route('payments.update',$payment->id)}}" accept-charset="UTF-8" id="edit_form">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">EDIT PAYMENTS</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @if (Auth::user()->type=='admin')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="control-label">PAYMENT FOR:*</label>
                                <select class="form-control select2" name="user_id">
                                    <option value="">SELECT</option>
                                    @foreach ($users as $item)
                                        <option value="{{$item->id}}" @if($payment->user_id==$item->id) selected @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">PAYMENT AMOUNT:*</label>
                            <input class="form-control" placeholder="PAYMENT AMOUNT" value="{{$payment->amount}}" autocomplete="off" name="amount" type="text">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">REMARKS:*</label>
                            <textarea class="form-control" placeholder="REMARKS" autocomplete="off" name="remarks">{{$payment->remarks}}</textarea>
                        </div>
                    </div>
                    @if (Auth::user()->type=='admin')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">STATUS:*</label>
                                <select class="form-control" name="status">
                                    <option value="1" @if ($payment->status==1) selected @endif>CONFIRMED</option>
                                    <option value="2" @if ($payment->status==2) selected @endif>PENDING</option>
                                </select>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save_button">UPDATE</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->