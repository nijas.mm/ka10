<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('payments.store')}}" accept-charset="UTF-8" id="createForm">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">ADD PAYMENTS</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
        
            <div class="modal-body">
                <div class="row">
                    @if (Auth::user()->type=='admin')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="control-label">PAYMENT FOR:*</label>
                                <select class="form-control select2" name="user_id">
                                    <option value="">SELECT</option>
                                    @foreach ($users as $item)
                                        <option value="{{$item->id}}" @if(Auth::id()==$item->id) selected @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">PAYMENT AMOUNT:*</label>
                            <input class="form-control" placeholder="PAYMENT AMOUNT" autocomplete="off" name="amount" type="text">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">REMARKS:*</label>
                            <textarea class="form-control" placeholder="REMARKS" autocomplete="off" name="remarks"></textarea>
                        </div>
                    </div>
                    @if (Auth::user()->type=='admin')
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">STATUS:*</label>
                                <select class="form-control" name="status">
                                    <option value="1">CONFIRMED</option>
                                    <option value="2">PENDING</option>
                                </select>
                            </div>
                        </div>
                    @endif
               </div> 
        
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save_button">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
    $(document).on('click','.add-product',function(e){
            var table = $("#product_table");
            var count = $("#product_table tbody tr").length;
            var count_table_tbody_tr = $("#attr_row_count").val();
            var row_id = (Number(count_table_tbody_tr) + 1);
            $.ajax({
                type: "POST",
                url: '{{route("getProducts")}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    // $('#product_value_id_'+row).html(response);
                    html = '<tr id="row_'+row_id+'"><td><select name="product_id[]" id="product_id_'+row_id+'" class="form-control select2"  style="width:100%" onchange="getAttributeValue('+row_id+')">'+response+'</select></td>' +
                    '<td><select name="product_value_id[]" class="form-control select2" style="width:100%" id="product_value_id_'+row_id+'" required><option value="">SELECT</option></select></td>' +
                    '<td><a class="btn btn-danger remove-product" onclick="removeRow('+row_id+')"><i class="fa fa-trash"></i></a></td>' +
                    '</tr>';
                    if (count >= 1) {
                        $("#product_table tbody tr:last").after(html);
                    } else {
                        $("#product_table tbody").html(html);
                    }
                    $('#product_id_'+row_id).html(response);
                },
            });
            $("#attr_row_count").val(row_id);
        });
function removeRow(tr_id){
    $("#product_table tbody tr#row_"+tr_id).remove();
}
</script>
