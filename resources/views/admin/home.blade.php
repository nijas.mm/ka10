<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>TECHTEA</title>
    <meta name="description" content="TECHTEA  - TAKE A BREAK BY NIJAS">
    <meta name="keywords" content="TECHTEA, TECHIE, IROID" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    
    <link type="text/css" rel="stylesheet" href="{{asset('assets/toastr.css')}}">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <img src="assets/img/loading-icon.png" alt="icon" class="loading-icon">
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-primary text-light">
        <div class="left">
            <a href="#" class="headerButton" data-bs-toggle="modal" data-bs-target="#sidebarPanel">
                <ion-icon name="menu-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            <img src="assets/img/logo.png" alt="logo" class="logo">
        </div>
        <div class="right">
            <a href="#" class="headerButton">
                <ion-icon class="icon" name="notifications-outline"></ion-icon>
                <span class="badge badge-danger">4</span>
            </a>
            <a href="#" class="headerButton">
                <img src="{{Auth::user()->profile_photo}}" alt="image" class="imaged w32">
                <span class="badge badge-danger">6</span>
            </a>
        </div>
    </div>
    <!-- * App Header -->


    <!-- App Capsule -->
    <div id="appCapsule">

        <!-- Wallet Card -->
        <div class="section wallet-card-section pt-1">
            <div class="wallet-card">
                <!-- Balance -->
                <div class="balance">
                    <div class="left">
                        <span class="title">Total Due</span>
                        <h1 class="total text-danger">₹ {{$total_due}}</h1>
                    </div>
                    <div class="right">
                        <a href="#" class="button" data-bs-toggle="modal" data-bs-target="#orderActionSheet">
                            <ion-icon name="add-outline"></ion-icon>
                        </a>
                    </div>
                </div>
                <!-- * Balance -->
            </div>
        </div>
        <!-- Wallet Card -->

        <!-- Order Action Sheet -->
        <div class="modal fade action-sheet" id="orderActionSheet" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Order</h5>
                    </div>
                    <div class="modal-body">
                        <div class="action-sheet-content">
                            <form id="order-form">
                              @csrf
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="label" for="account1">Item</label>
                                        <select class="form-control custom-select" id="product_id" name="product_id">
                                            <option value="">SELECT ITEM</option>
                                            @foreach ($products as $item)
                                                <option value="{{$item->id}}">{{$item->name}} - ₹ {{$item->price}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group basic">
                                    <label class="label">Quantity</label>
                                    <select class="form-control custom-select" id="qty" name="qty">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                        </select>
                                </div>
                                <div class="form-group basic">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg save_button">Order</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Order Action Sheet -->

        <!-- Stats -->
        <div class="section">
            <div class="row mt-2">
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Orders</div>
                        <div class="value text-danger">{{$total_orders}}</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Amount</div>
                        <div class="value text-danger">₹ {{$total_amount}}</div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Paid</div>
                        <div class="value text-success">₹ {{$total_paid}}</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="stat-box">
                        <div class="title">Total Discounts</div>
                        <div class="value text-success">₹ {{$total_contribution}}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- * Stats -->

        <!-- Transactions -->
        <div class="section mt-4">
            <div class="section-heading">
                <h2 class="title">Orders</h2>
                <a href="#" class="link">View All</a>
            </div>
            <div class="transactions">
                <!-- item -->
                @foreach ($transactions as $item)
                    <a href="#" class="item">
                    <div class="detail">
                        <img src="{{$item->image}}" alt="img" class="image-block imaged w48">
                        <div>
                            <strong>{{$item->name}}</strong>
                            <p>{{$item->date}}</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price text-danger"> ₹ {{$item->net_amount}}</div>
                    </div>
                </a>
                @endforeach
                @if((isset($transactions) && count($transactions)==0))
                No Orders
                @endif
            </div>
        </div>
        <!-- * Transactions -->

        <!-- Transactions -->
        <div class="section mt-4">
            <div class="section-heading">
                <h2 class="title">Payments</h2>
                <a href="#" class="link">View All</a>
            </div>
            <div class="transactions">
                <!-- item -->
                @foreach ($payments as $item)
                    <a href="#" class="item">
                    <div class="detail">
                        <img src="img/paid.jpg" alt="img" class="image-block imaged w48">
                        <div>
                            <strong>Paid On</strong>
                            <p>{{$item->date}}</p>
                        </div>
                    </div>
                    <div class="right">
                        <div class="price text-success"> ₹ {{$item->net_amount}}</div>
                    </div>
                </a>
                @endforeach
                @if((isset($payments) && count($payments)==0))
                No Payments
                @endif
            </div>
        </div>
        <!-- * Transactions -->

        <!-- Monthly Bills -->
        <div class="section full mt-4">
            <div class="section-heading padding">
                <h2 class="title">Available Items</h2>
                <a href="#" class="link">View All</a>
            </div>
            <!-- carousel multiple -->
            <div class="carousel-multiple splide">
                <div class="splide__track">
                    <ul class="splide__list">

                        @foreach ($products as $item)
                          <li class="splide__slide">
                            <div class="bill-box">
                                <div class="img-wrapper">
                                    <img src="{{$item->image}}" alt="img" class="image-block imaged w48">
                                </div>
                                <div class="price">₹ {{$item->price}}</div>
                                <p>{{$item->name}}</p>
                                <a href="#" class="btn btn-primary btn-block btn-sm" data-bs-toggle="modal" onclick="orderItem({{$item->id}})" data-bs-target="#orderActionSheet">ORDER</a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- * carousel multiple -->
        </div>
        <!-- * Monthly Bills -->

        <!-- app footer -->
        <div class="appFooter">
            <div class="footer-title">
                Copyright © TECHTEA {{date('Y')}}. All Rights Reserved.
            </div>
        </div>
        <!-- * app footer -->

    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
    <div class="appBottomMenu">
        <a href="#" class="item active">
            <div class="col">
                <ion-icon name="pie-chart-outline"></ion-icon>
                <strong>Overview</strong>
            </div>
        </a>
        <a href="{{route('orders')}}" class="item">
            <div class="col">
                <ion-icon name="document-text-outline"></ion-icon>
                <strong>Orders</strong>
            </div>
        </a>
        <a href="#" class="item">
            <div class="col">
                <ion-icon name="apps-outline"></ion-icon>
                <strong>Items</strong>
            </div>
        </a>
        <a href="#" class="item">
            <div class="col">
                <ion-icon name="card-outline"></ion-icon>
                <strong>Payments</strong>
            </div>
        </a>
        <a href="#" class="item">
            <div class="col">
                <ion-icon name="settings-outline"></ion-icon>
                <strong>Settings</strong>
            </div>
        </a>
    </div>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <!-- profile box -->
                    <div class="profileBox pt-2 pb-2">
                        <div class="image-wrapper">
                            <img src="{{Auth::user()->profile_photo}}" alt="image" class="imaged  w36">
                        </div>
                        <div class="in">
                            <strong>{{Auth::user()->name}}</strong>
                            <div class="text-muted">{{Auth::user()->phone}}</div>
                        </div>
                        <a href="#" class="btn btn-link btn-icon sidebar-close" data-bs-dismiss="modal">
                            <ion-icon name="close-outline"></ion-icon>
                        </a>
                    </div>
                    <!-- * profile box -->
                    <!-- balance -->
                    <div class="sidebar-balance">
                        <div class="listview-title">Total Due</div>
                        <div class="in">
                            <h1 class="amount">₹ {{$total_due}}</h1>
                        </div>
                    </div>
                    <!-- * balance -->

                    <!-- menu -->
                    <div class="listview-title mt-1">Menu</div>
                    <ul class="listview flush transparent no-line image-listview">
                        <li>
                            <a href="{{url('/')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="pie-chart-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Overview
                                    <span class="badge badge-primary">10</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('orders')}}" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="document-text-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Orders
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="apps-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Items
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="card-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Payments
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- * menu -->

                    <!-- others -->
                    <div class="listview-title mt-1">Others</div>
                    <ul class="listview flush transparent no-line image-listview">
                        <li>
                            <a href="#" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="settings-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Settings
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="chatbubble-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Support
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void" class="item" onclick="$('#logout-form').submit();">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="log-out-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Log out
                                </div>
                            </a>
                        </li>
                    </ul>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <!-- * others -->
                </div>
            </div>
        </div>
    </div>
    <!-- * App Sidebar -->
    <!-- ========= JS Files =========  -->
    <!-- Bootstrap -->
    <script src="assets/js/lib/bootstrap.bundle.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <!-- Splide -->
    <script src="assets/js/plugins/splide/splide.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    
    <script src="{{asset('assets/toastr.min.js')}}"></script>
    <script>
      function orderItem(id){
        $("#product_id").val(id).trigger('change');
      }
      $("#order-form").unbind('submit').on('submit', function (e) {
      $('.save_button').text('Please Wait...!');
      $(this).find('button[type="submit"]').attr('disabled', true);
      e.preventDefault();
      var data = $(this).serialize();
      $.ajax({
          method: 'POST',
          url: "{{route('app-order')}}",
          dataType: 'json',
          data: data,
          success: function(result) {
              if (result.status === true) {
                  toastr.success(result.message);
                  window.setTimeout(function(){
                      window.location.href = "/";
                    }, 3000);
              } else {
                  toastr.error(result.message);
                  $('.save_button').attr('disabled', false);
                  $('.save_button').text('Get me In!');
              }
          },
      });
  });
    </script>
</body>

</html>