<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('products.store')}}" accept-charset="UTF-8" id="createForm" accept="multipart/form-data">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">ADD PRODUCT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
        
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">PRODUCT NAME:*</label>
                            <input class="form-control" required="" placeholder="PRODUCT NAME" autocomplete="off" name="name" type="text" id="name" >
                        </div>  
                        <div class="form-group">
                            <label for="name" class="control-label">PRODUCT PRICE:*</label>
                            <input class="form-control" required="" placeholder="PRODUCT PRICE" autocomplete="off" name="price" type="text" id="price" >
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label">IMAGE:*</label>
                            <input  class="form-control" required="" placeholder="IMAGE" autocomplete="off" name="image" type="file" id="image" value="">
                        </div> 
                        <div class="form-group">
                            <label for="name">STATUS:*</label>
                            <select class="form-control" name="status">
                                <option value="1">ACTIVE</option>
                                <option value="2">INACTIVE</option>
                            </select>
                        </div>
                        
                    </div> 
               </div> 
        
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save_button">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
