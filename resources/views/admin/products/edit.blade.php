<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="POST" action="{{route('products.update',$product->id)}}" accept-charset="UTF-8" id="edit_form" accept="multipart/form-data">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">EDIT PRODUCT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">PRODUCT NAME:*</label>
                            <input  class="form-control" placeholder="PRODUCT NAME" autocomplete="off" name="name" type="text" id="name" value="{{$product->name}}">
                        </div>  
                        <div class="form-group">
                            <label for="name" class="control-label">PRODUCT PRICE:*</label>
                            <input class="form-control" placeholder="PRODUCT PRICE" autocomplete="off" name="price" type="text" id="price" value="{{$product->price}}">
                        </div>
                        <div class="form-group">
                            <label for="name" class="control-label">IMAGE:*</label>
                            <img src="{{asset($product->image)}}" style="height:50px" alt="">
                            <input  class="form-control"  placeholder="IMAGE" autocomplete="off" name="image" type="file" id="image" value="" >
                        </div> 
                        <div class="form-group">
                            <label for="name">STATUS:*</label>
                            <select class="form-control" name="status">
                                <option value="1" @if($product->status == 1) selected @endif>ACTIVE</option>
                                <option value="2" @if($product->status == 2) selected @endif>INACTIVE</option>
                            </select>
                        </div>
                        
                    </div>
               </div> 
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save_button">UPDATE</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->