<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="POST" action="{{route('category.update',$category->id)}}" accept-charset="UTF-8" id="edit_form">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">EDIT CATEGORY</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">CATEGORY NAME:*</label>
                            <input class="form-control" required="" placeholder="CATEGORY NAME" autocomplete="off" value="{{$category->name}}" name="name" type="text" id="name" >
                        </div>   
                    </div> 
               </div> 
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save_button">UPDATE</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->