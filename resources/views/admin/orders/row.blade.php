<tr id="row_{{$row_count}}">
    <td>
        <select name="product_id[]" id="product_id_{{$row_count}}" class="form-control select2" onchange="getProductValue({{$row_count}})" style="width:100%">
            <option value="">SELECT</option>
            @foreach ($products as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </td>
    <td><input type="text" class="form-control" onchange="getProductValue({{$row_count}})"  name="qty[]" id="qty_{{$row_count}}" value="1"></td>
    <td id="price_{{$row_count}}"><input readonly type="text" class="form-control" name="price[]" id="price_{{$row_count}}" value="0"></td>
    <td id="total_{{$row_count}}"><input readonly type="text" class="form-control" name="total[]" id="total_{{$row_count}}" value="0"></td>
    <td><a class="btn btn-danger remove-attribute" onclick="removeRow({{$row_count}})"><i class="fa fa-trash"></i></a></td>
</tr>   

<script>
    function getProductValue(rw_id){ 
        var prodId = $('#product_id_'+rw_id).val();   
        var qty    = Number($('#qty_'+rw_id).val()); 
        $.ajax({
            type: "POST",
            url: '{{route("getProductValue")}}',
            dataType: "json",
            data: {
                "_token": "{{ csrf_token() }}",
                prodId:prodId
            },
            success: function(res) { 
                var price = res.price;
                var total = price*qty;
              
                
                $('input#price_'+rw_id).val(price); 
                $('input#total_'+rw_id).val(total);
            },
        });   
    }
</script>