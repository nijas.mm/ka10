<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('orders.store')}}" accept-charset="UTF-8" id="createForm">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">ADD ORDER</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
        
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="control-label">ORDER FOR:*</label>
                            <select class="form-control select2" name="user_id">
                                <option value="">SELECT</option>
                                @foreach ($users as $item)
                                    <option value="{{$item->id}}" @if(Auth::id()==$item->id) selected @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>  
                    </div>
                    <div class="col-md-12"> 
                                <table class="table table-bordered" id="product_table">
                                    <thead>
                                        <tr>
                                            <td style="width: 40%">ITEM</td>
                                            <td>QTY</td>
                                            <td>PRICE</td>
                                            <td>TOTAL</td>
                                            <td><a class="btn btn-success add-product" onclick="rowAdd()"><i class="fa fa-plus"></i></a>
                                        </tr>
                                    </thead>
                                    <tbody id="orderBody"> 
                                        @include('admin.orders.row')
                                    <tbody>
                                </table>
                                <input type="hidden" name="" id="row_count" value="1">
                    </div> 
               </div> 
        
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save_button">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
function rowAdd(){
    var rowCount = Number($('#row_count').val()) + 1;
    $('#row_count').val(rowCount);
    $.ajax({
        type: "POST",
        url: '{{route("getAppendedRows")}}',
        data: {
            "_token": "{{ csrf_token() }}",
            row_count:rowCount
        },
        success: function(res) {
            
            $('#orderBody').append(res.html); 
        },
    });
}
function removeRow(tr_id){
    $("#product_table tbody tr#row_"+tr_id).remove();
}

</script>
