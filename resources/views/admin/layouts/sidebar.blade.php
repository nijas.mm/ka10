@include('admin.layouts.navbar')
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link text-center">
      {{-- <img src="{{$settings->logo}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
      <span class="brand-text font-weight-light">ADMIN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('img/user2.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{route('change_password')}}" class="d-block">CHANGE PASSWORD</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         
            
          <li class="nav-item">
            <a href="{{url('admin/settings')}}" class="nav-link">
                <i class="nav-icon fa fa-cog"></i>
                <p>DASHBOARD</p>
            </a>
          </li>
          <li class="nav-item"><a href="{{route('orders.index')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>ORDERS</p></a></li>
          <li class="nav-item"><a href="{{route('payments.index')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>PAYMENTS</p></a></li>
          <li class="nav-item"><a href="{{route('products.index')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>PRODUCTS</p></a></li>
          <li class="nav-item"><a href="{{route('users.index')}}" class="nav-link"><i class="far fa-circle nav-icon"></i><p>USERS</p></a></li>
          <li class="nav-item">
            <a href="javascript:void" class="nav-link" onclick="$('#logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>LOGOUT</p>
            </a>
          </li>
          
        </ul>
       
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>