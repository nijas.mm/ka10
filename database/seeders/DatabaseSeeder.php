<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            'id' => 1,
            'name' => 'IROID TECHNOLOGIES',
            'start_time' => '2021-11-30 12:30:00',
            'end_time' => '2021-11-30 14:30:00',
            'invoice_prefix' => 'IROID#',
            'invoice_number' => '1',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'id' => 1,
            'type' => 'admin',
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'phone' => '8123456789',
            'profile_photo' => 'img/user2.png',
            'password' => Hash::make('0123'),
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        $this->call([PermissionsTableSeeder::class]);
    }
}
