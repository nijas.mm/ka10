<?php

use App\Http\Controllers\admin\CommonController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\admin\UsersController;
use App\Http\Controllers\admin\ProductsController;
use App\Http\Controllers\admin\OrderController;
use App\Http\Controllers\admin\PaymentsController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/orders', [App\Http\Controllers\HomeController::class, 'orders'])->name('orders');
Route::get('/payments', [App\Http\Controllers\HomeController::class, 'payments'])->name('payments');
Route::get('/items', [App\Http\Controllers\HomeController::class, 'items'])->name('items');
Route::get('/settings', [App\Http\Controllers\HomeController::class, 'settings'])->name('settings');
Route::get('/darkmode-switch/{mode}', [App\Http\Controllers\HomeController::class, 'darkmodeSwitch'])->name('darkmode-switch');
Route::post('user-login', [LoginController::class, 'userLogin'])->name('user-login');
Route::post('user-signup', [LoginController::class, 'userSignup'])->name('user-signup');

// Admin Routes
Route::get('/products', [App\Http\Controllers\HomeController::class, 'products'])->name('products');
Route::post('/create-product', [App\Http\Controllers\HomeController::class, 'createProduct'])->name('create-product');
Route::get('/all-orders', [App\Http\Controllers\HomeController::class, 'allOrders'])->name('all-orders');
Route::post('/create-order', [App\Http\Controllers\HomeController::class, 'createOrder'])->name('create-order');
Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'auth:web'], function () {
        Route::get('/settings', [HomeController::class, 'index']);
        Route::match(['get', 'post'], '/change-password', [SettingsController::class, 'change_password'])->name('change_password');
        Route::post('/update-settings', [SettingsController::class, 'update']);
        Route::resource('roles', RoleController::class);
        Route::resource('users', UsersController::class);
        Route::get('delete/{route}/{id}', [CommonController::class, 'delete'])->name('delete');
        // Route::resource('products', ProductsController::class);
        Route::post('get-product-value', [ProductsController::class, 'getProductValue'])->name('getProductValue');
        Route::resource('orders', OrderController::class);
        Route::resource('payments', PaymentsController::class);
        Route::post('get-append-rows', [OrderController::class, 'getAppendedRows'])->name('getAppendedRows');
        Route::get('getProducts', [ProductsController::class, 'getProducts'])->name('getProducts');
        Route::post('app-order', [OrderController::class, 'appOrder'])->name('app-order');
    });
});
